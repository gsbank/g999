Sample configuration files for:

SystemD: G999d.service
Upstart: G999d.conf
OpenRC:  G999d.openrc
         G999d.openrcconf
CentOS:  G999d.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
