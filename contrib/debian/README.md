
Debian
====================
This directory contains files used to package G999d/G999-qt
for Debian-based Linux systems. If you compile G999d/G999-qt yourself, there are some useful files here.

## G999: URI support ##


G999-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install G999-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your G999qt binary to `/usr/bin`
and the `../../share/pixmaps/G999128.png` to `/usr/share/pixmaps`

G999-qt.protocol (KDE)

