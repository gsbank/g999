G999 Core integration/staging tree
=====================================

GSB is delivering sophisticated, priceless software trading modules, platforms and signal / data streaming to the financial industry to enhance their services towards their very own clientele. Fully white labeled and customizable. 

More information at [G999.io](https://www.G999.io)

