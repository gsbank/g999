#include "masternodesendconfig.h"
#include "ui_masternodesendconfig.h"

#include "bitcoinunits.h"
#include "guiconstants.h"
#include "init.h"
#include "optionsmodel.h"
#include "walletmodel.h"

#include <QKeyEvent>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>

MasternodesendConfig::MasternodesendConfig(QWidget* parent) : QDialog(parent),
                                                        ui(new Ui::MasternodesendConfig),
                                                        model(0)
{
    ui->setupUi(this);

    connect(ui->buttonBasic, SIGNAL(clicked()), this, SLOT(clickBasic()));
    connect(ui->buttonHigh, SIGNAL(clicked()), this, SLOT(clickHigh()));
    connect(ui->buttonMax, SIGNAL(clicked()), this, SLOT(clickMax()));
}

MasternodesendConfig::~MasternodesendConfig()
{
    delete ui;
}

void MasternodesendConfig::setModel(WalletModel* model)
{
    this->model = model;
}

void MasternodesendConfig::clickBasic()
{
    configure(true, 1000, 2);

    QString strAmount(BitcoinUnits::formatWithUnit(
        model->getOptionsModel()->getDisplayUnit(), 1000 * COIN));
    QMessageBox::information(this, tr("Masternodesend Configuration"),
        tr(
            "Masternodesend was successfully set to basic (%1 and 2 rounds). You can change this at any time by opening G999's configuration screen.")
            .arg(strAmount));

    close();
}

void MasternodesendConfig::clickHigh()
{
    configure(true, 1000, 8);

    QString strAmount(BitcoinUnits::formatWithUnit(
        model->getOptionsModel()->getDisplayUnit(), 1000 * COIN));
    QMessageBox::information(this, tr("Masternodesend Configuration"),
        tr(
            "Masternodesend was successfully set to high (%1 and 8 rounds). You can change this at any time by opening G999's configuration screen.")
            .arg(strAmount));

    close();
}

void MasternodesendConfig::clickMax()
{
    configure(true, 1000, 16);

    QString strAmount(BitcoinUnits::formatWithUnit(
        model->getOptionsModel()->getDisplayUnit(), 1000 * COIN));
    QMessageBox::information(this, tr("Masternodesend Configuration"),
        tr(
            "Masternodesend was successfully set to maximum (%1 and 16 rounds). You can change this at any time by opening G999's configuration screen.")
            .arg(strAmount));

    close();
}

void MasternodesendConfig::configure(bool enabled, int coins, int rounds)
{
    QSettings settings;

    settings.setValue("nMasternodesendRounds", rounds);
    settings.setValue("nAnonymizeG999Amount", coins);

    nMasternodesendRounds = rounds;
    nAnonymizeG999Amount = coins;
}
