#ifndef MASTERNODESENDCONFIG_H
#define MASTERNODESENDCONFIG_H

#include <QDialog>

namespace Ui
{
class MasternodesendConfig;
}
class WalletModel;

/** Multifunctional dialog to ask for passphrases. Used for encryption, unlocking, and changing the passphrase.
 */
class MasternodesendConfig : public QDialog
{
    Q_OBJECT

public:
    MasternodesendConfig(QWidget* parent = 0);
    ~MasternodesendConfig();

    void setModel(WalletModel* model);


private:
    Ui::MasternodesendConfig* ui;
    WalletModel* model;
    void configure(bool enabled, int coins, int rounds);

private slots:

    void clickBasic();
    void clickHigh();
    void clickMax();
};

#endif // MASTERNODESENDCONFIG_H
